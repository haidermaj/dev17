/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.teamproject.airviasystemnew;

import com.teamproject.airviasystem.*;

import java.sql.*;

/**
 *
 * @author Haider
 */
public class GUIDatabaseHandler {

    public Connection connect() {
        Connection conn = null;
        try {
            // db parameters
            String url = "jdbc:sqlite:" + System.getenv("USERPROFILE") + "\\repos\\dev17\\db\\teamProject.db";
            System.out.println(url);
            // create a connection to the database
            conn = DriverManager.getConnection(url);

            System.out.println("Connection to SQLite has been established.");

        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        /*
        finally {
            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
            }
        }

         */
        return conn;
    }

    public String[] logInDetails(String sql) {

        String[] logInDetails = new String[3];
        int id = 0;
        try (Connection conn = this.connect();
             Statement stmt = conn.createStatement();
             ResultSet rs = stmt.executeQuery(sql)) {

            while (rs.next()) {
                logInDetails[0] = rs.getString("username");
                logInDetails[1] = rs.getString("password");
                id = rs.getInt("id");
            }
            logInDetails[2] = Integer.toString(id);

        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return logInDetails;
    }

    public String[] accountPageQuery(String sql) {
        String[] accountQuery = new String[3];
        int role = 0;
        try (Connection conn = this.connect();
             Statement stmt = conn.createStatement();
             ResultSet rs = stmt.executeQuery(sql)) {
            while (rs.next()) {
                accountQuery[0] = rs.getString("username");
                role = rs.getInt("userRole");
                accountQuery[1] = Integer.toString(role);
                accountQuery[2] = rs.getString("total_blanks");
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return accountQuery;
    }
    /*
    public String [] latePaymentQuery(String sql) {
        String []
    }
    */
    public void customerRegIns(String firstName, String lastName, String email, String telephone, String address, String dob, int customerStatus, String passport_id) {
        String sql = "Insert into Customer (firstName, lastName, email, telephone, dateOfBirth, address, customerStatus,  passport_id) VALUES(?,?,?,?,?,?,?,?)";

        try (Connection conn = this.connect();
             PreparedStatement pstmt = conn.prepareStatement(sql)) {
            pstmt.setString(1, firstName);
            pstmt.setString(2, lastName);
            pstmt.setString(3, email);
            pstmt.setString(4, telephone);
            pstmt.setString(5, address);
            pstmt.setString(6, dob);
            pstmt.setInt(7, customerStatus);
            pstmt.setString(8, passport_id);
            pstmt.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }

    public void employeeRegIns(int id, String firstName, String lastName, String username, String password, String email, String telephone, String address, int userRole) {
        String sql = "Insert into Employee (id, firstName, lastName, username, password, email, telephone, address, userRole) VALUES(?,?,?,?,?,?,?,?,?)";

        try (Connection conn = this.connect();
             PreparedStatement pstmt = conn.prepareStatement(sql)) {
            pstmt.setInt(1, id);
            pstmt.setString(2, firstName);
            pstmt.setString(3, lastName);
            pstmt.setString(4, username);
            pstmt.setString(5, password);
            pstmt.setString(6, email);
            pstmt.setString(7, telephone);
            pstmt.setString(8, address);
            pstmt.setInt(9, userRole);

            pstmt.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }
    //query for blirnback ticketId
    public int queryTicketId(String sql) {
        int ticketId = 0;
        try (Connection conn = this.connect();
             Statement stmt = conn.createStatement();
             ResultSet rs = stmt.executeQuery(sql)) {

            while (rs.next()) {
                ticketId = rs.getInt("id");
            }

        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return ticketId;
    }


    //query for refund
    public void queryRefund(int ticketId, int refundAmount, Date dateOfRefund, String refundStatus, String refundDesc){
        String sql = "INSERT into refund (ticketId, refundAmount, dateOfRefund, refundStatus, refundDesc) values(?, ?, ?, ?, ?) ";
        try(Connection conn = this.connect();
        PreparedStatement pstmt = conn.prepareStatement(sql)){
            pstmt.setInt(1, ticketId);
            pstmt.setInt(2,refundAmount);
            pstmt.setDate(3,dateOfRefund);
            pstmt.setString(4, refundStatus);
            pstmt.setString(5,refundDesc);
        }catch (SQLException e){
            System.out.println(e.getMessage());
        }
    }

    //update commission rate
    public void updateCommissionRate(int id, int commission ) {
        String sql = "UPDATE bundlesType SET name = ? , "
                + "commission ? "
                + "WHERE id = ?";

        try (Connection conn = this.connect();
             PreparedStatement pstmt = conn.prepareStatement(sql)) {

            // set the corresponding param
            pstmt.setInt(1, commission);
            pstmt.setInt(2, id);
            // update
            pstmt.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }

    public String queryUsernameComparison(String sql) {
        String result = null;
        try (Connection conn = this.connect();
        Statement stmt = conn.createStatement();
        ResultSet rs = stmt.executeQuery(sql)) {
            while(rs.next()){
                result = rs.getString("username");
                System.out.println(result);
            }
        } catch (Exception e) {
            System.out.println(e);
        }
        return result;
    }

    public String queryEmailComparison(String sql) {
        String result = null;
        try (Connection conn = this.connect();
             Statement stmt = conn.createStatement();
             ResultSet rs = stmt.executeQuery(sql)) {
            while(rs.next()){
                result = rs.getString("email");
                System.out.println(result);
            }
        } catch (Exception e) {
            System.out.println(e);
        }
        return result;
    }


    public int queryIDComparison(String sql) {
        int id = -1;
        try (Connection conn = this.connect();
             Statement stmt = conn.createStatement();
             ResultSet rs = stmt.executeQuery(sql)) {
            while(rs.next()){
                id = rs.getInt("id");
            }
        } catch (Exception e) {
            System.out.println(e);
        }
        return id;
    }

    public int queryAccessLevel(int id){
        int role = -1;
        try (Connection conn = this.connect();
             Statement stmt = conn.createStatement();
             ResultSet rs = stmt.executeQuery("select userRole from Employee where id = " + id)) {
             return rs.getInt("userRole");
        } catch (Exception e) {
            System.out.println(e);
        }
        return role;
    }
    
    public float queryReportInterlinePerAdvisor(){       // hey we need the dates
        float x = 0;
        try (Connection conn = this.connect();
               Statement stmt =conn.createStatement();
               ResultSet rs = stmt.executeQuery("SELECT sales.employeeId,\n" +
                       "       count(tickets.bundlesId) AS ticketNbr,\n" +
                       "       sum(sales.cost) AS cost,\n" +
                       "       sum(round( (sales.cost * exchangeRate.value), 2) ) AS fareAmount,\n" +
                       "       sum(sales.taxesLz) AS TaxesLz,\n" +
                       "       sum(sales.taxesOther) AS taxesOther,\n" +
                       "       sum(sales.cost + sales.taxesLz + sales.taxesOther) AS totalCost,\n" +
                       "       sum(sales.cost) * bundlesType.commission / 100 AS travelAgentProfit,\n" +
                       "       sum(sales.nonAssessAmount) AS NonAssessAmount,\n" +
                       "       sum(sales.cost + sales.taxesLz + sales.taxesOther) - (sum(sales.cost) * bundlesType.commission / 100) AS AirVIaPr \n" +
                       "  FROM sales\n" +
                       "       INNER JOIN\n" +
                       "       tickets ON sales.ticketId = tickets.id\n" +
                       "       INNER JOIN\n" +
                       "       exchangeRate ON sales.exchangeRateId = exchangeRate.id\n" +
                       "       INNER JOIN\n" +
                       "       bundles ON tickets.bundlesId = bundles.id\n" +
                       "       INNER JOIN\n" +
                       "       bundlesType ON bundles.bundleTypeId = bundlesType.id\n" +
                       " WHERE sales.saleType = 1\n" +
                       " GROUP BY sales.employeeId;\n"))   {

                    while (rs.next()){
                        System.out.println(rs.getInt("AirViaPr"));
                    }
        }catch (Exception e){
            System.out.println(e);
        }

        return x;
    }
    
    public void ticketInfoIns(String bundlesId, String customerId, String cityOfDeparture, String cityOfArrival, String boardingTime, String dateOfFlight, String gate){
        String sql = "INSERT INTO tickets (\n" +
                "\n" +
                "                        bundlesId,\n" +
                "                        customerId,\n" +
                "                        status,\n" +
                "                        cityOfDeparture,\n" +
                "                        cityOfArrival,\n" +
                "                        boardingTime,\n" +
                "                        dateOfFlight,\n" +
                "                        gate\n" +
                "                    )\n" +
                "                    VALUES (\n" +
                "                        ?,\n" +
                "                        ?,\n" +
                "                        ?,\n" +
                "                        ?,\n" +
                "                        ?,\n" +
                "                        ?,\n" +
                "                        ?,\n" +
                "                        ?\n" +
                "                    );";
        try (Connection conn = this.connect();
        PreparedStatement pstmt = conn.prepareStatement(sql)) {
            pstmt.setInt(1,Integer.parseInt(bundlesId));
            pstmt.setInt(2,Integer.parseInt(customerId));
            pstmt.setInt(3,1);
            pstmt.setString(4,cityOfDeparture);
            pstmt.setString(5, cityOfArrival);
            pstmt.setTime(6, Time.valueOf(boardingTime));
            pstmt.setDate(7, Date.valueOf(new DateHandler().reverseDate(dateOfFlight)));
            pstmt.setString(8, gate);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void salesInfoIns(String ticketId, String customerId, String employeeId, String saleType, String cost, String currencies, String taxesLz, String taxesOther,
                 String taxesDom, String dateOfSale, String paymentMethod, String latePayment, String nonAssessAmount,
                             String exchangeRateId, String cardHolderName, String cardNumber, String cardExpiryDate, String cardCVVNumber) {
        String sql = "INSERT INTO sales (\n" +
                "\n" +
                "                      ticketId,\n" +
                "                      customerId,\n" +
                "                      employeeId,\n" +
                "                      saleType,\n" +
                "                      cost,\n" +
                "                      currencies,\n" +
                "                      taxesLz,\n" +
                "                      taxesOther,\n" +
                "                      taxesDom,\n" +
                "                      dateOfSale,\n" +
                "                      paymentDate,\n" +
                "                      paymentMethod,\n" +
                "                      latePayment,\n" +
                "                      nonAssessAmount,\n" +
                "                      exchangeRateId,\n" +
                "                      creditFullName,\n" +
                "                      creditNumber,\n" +
                "                      creditExpDate,\n" +
                "                      creditCVV\n" +
                "                  )\n" +
                "                  VALUES (\n" +
                "                      ?,\n" +
                "                      ?,\n" +
                "                      ?,\n" +
                "                      ?,\n" +
                "                      ?,\n" +
                "                      ?,\n" +
                "                      ?,\n" +
                "                      ?,\n" +
                "                      ?,\n" +
                "                      ?,\n" +
                "                      ?,\n" +
                "                      ?,\n" +
                "                      ?,\n" +
                "                      ?,\n" +
                "                      ?,\n" +
                "                      ?,\n" +
                "                      ?,\n" +
                "                      ?,\n" +
                "                      ?\n" +
                "                  );";
        try(Connection conn = this.connect();
        PreparedStatement pstmt = conn.prepareStatement(sql)) {
            pstmt.setInt(1, Integer.parseInt(ticketId));
            pstmt.setInt(2, Integer.parseInt(customerId));
            pstmt.setInt(3, Integer.parseInt(employeeId));
            pstmt.setInt(4, Integer.parseInt(saleType));
            pstmt.setDouble(5, Double.parseDouble(cost));
            pstmt.setString(6, currencies);
            pstmt.setDouble(7, Double.parseDouble(taxesLz));
            pstmt.setDouble(8, Double.parseDouble(taxesOther));
            pstmt.setDouble(9, Double.parseDouble(taxesDom));
            pstmt.setDate(10, Date.valueOf(new DateHandler().reverseDate(dateOfSale)));
            pstmt.setDate(11, Date.valueOf(new DateHandler().reverseDate(new DateHandler().incrementMonth())));
            pstmt.setString(12, paymentMethod);
            pstmt.setInt(13, Integer.parseInt(latePayment));
            pstmt.setDouble(14, Double.parseDouble(nonAssessAmount));
            pstmt.setInt(15, Integer.parseInt(exchangeRateId));
            pstmt.setString(16,cardHolderName);
            pstmt.setString(17,cardNumber);
            pstmt.setDate(18, Date.valueOf(cardExpiryDate));
            pstmt.setString(19,cardCVVNumber);
        } catch (Exception e){
            e.printStackTrace();
        }
    }


    public int queryExchangeRateId(){
        String sql = "SELECT id\n" +
                "  FROM exchangeRate\n" +
                " ORDER BY id DESC\n" +
                " LIMIT 1;";
        try(Connection conn = this.connect();
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql)) {
            return rs.getInt("id");
        } catch (Exception e){
            e.printStackTrace();
        }
        return -1;
    }

    public int queryTicketsId(){
        String sql = "SELECT id\n" +
                "  FROM tickets\n" +
                " ORDER BY id DESC\n" +
                " LIMIT 1;";
        try(Connection conn = this.connect();
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql)) {
            return rs.getInt("id");
        } catch (Exception e){
            e.printStackTrace();
        }
        return -1;
    }

}
