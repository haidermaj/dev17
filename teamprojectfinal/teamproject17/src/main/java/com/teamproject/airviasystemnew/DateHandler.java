/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.teamproject.airviasystemnew;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 *
 * @author Haider
 */
public class DateHandler {
    
    public String getDate(){
        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        return dateFormat.format(new Date());
    }

    //Method to produce a generated name for the reports pdfs
    public String getReportDate(){
        //Return day, month, year, hour, minute and second to make a unique report pdf name
        DateFormat dateFormat = new SimpleDateFormat("ddMMyyyhhmms");
        return dateFormat.format(new Date());
    }

    public String reverseDate(String date){
        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
        return dateFormat.format(date);
    }

    public String incrementMonth() {
        Calendar nextMonth = Calendar.getInstance();
        nextMonth.add(Calendar.MONTH, 1);
        return nextMonth.toString();
    }

}
