package com.teamproject.airviasystemnew;
import com.itextpdf.text.Document;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfWriter;

import java.awt.*;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

public class PDFGenerator {
    private String fileLocation;

    public PDFGenerator(String fileLocation){this.fileLocation = fileLocation;}

    public String getFileLocation() {return fileLocation;}
    public void setFileLocation(String fileLocation) {this.fileLocation = fileLocation;}

    private String generateFileName() {return fileLocation + "\\" + "ATS_" + new DateHandler().getReportDate() + ".pdf";}

    public void generateTicketStockTurnoverReport(){
        try {
            String filePath = generateFileName();
            Document report = new Document();
            PdfWriter.getInstance(report,new FileOutputStream(filePath));
            report.open();
            report.add(new Paragraph("Ticket Stock Turnover Report Sample Text"));
            report.close();
            openReportPDF(filePath);
        } catch (Exception e){
            System.out.println("Failed to generate Ticket Stock Turnover Report.");
            e.printStackTrace();
        }
    }
    public void generateInterlineIndividualSalesReport() {
        try {
            String filePath = generateFileName();
            Document report = new Document();
            PdfWriter.getInstance(report,new FileOutputStream(filePath));
            report.open();
            report.add(new Paragraph("Interline Individual Sales Report Sample Text"));
            report.close();
            openReportPDF(filePath);
        } catch (Exception e){
            System.out.println("Failed to generate Interline Individual Sales Report.");
            e.printStackTrace();
        }
    }
    public void generateInterlineGlobalSalesReport() {
        try {
            String filePath = generateFileName();
            Document report = new Document();
            PdfWriter.getInstance(report,new FileOutputStream(filePath));
            report.open();
            report.add(new Paragraph("Global Sales Report Sample Text"));
            report.close();
            openReportPDF(filePath);
        } catch (Exception e){
            System.out.println("Failed to generate Global Sales Report.");
            e.printStackTrace();
        }
    }
    public void generateDomesticIndividualSalesReport() {
        try {
            String filePath = generateFileName();
            Document report = new Document();
            PdfWriter.getInstance(report,new FileOutputStream(filePath));
            report.open();
            report.add(new Paragraph("Domestic Individual Sales Report Sample Text"));
            report.close();
            openReportPDF(filePath);
        } catch (Exception e){
            System.out.println("Failed to generate Domestic Individual Sales Report.");
            e.printStackTrace();
        }
    }
    public void generateDomesticGlobalSalesReport() {
        try {
            String filePath = generateFileName();
            Document report = new Document();
            PdfWriter.getInstance(report,new FileOutputStream(filePath));
            report.open();
            report.add(new Paragraph("Domestic Global Sales Report Sample Text"));
            report.close();
            openReportPDF(filePath);
        } catch (Exception e){
            System.out.println("Failed to generate Domestic Global Sales Report.");
            e.printStackTrace();
        }
    }
    public void generateGlobalPerUSDReport() {
        try {
            String filePath = generateFileName();
            Document report = new Document();
            PdfWriter.getInstance(report,new FileOutputStream(filePath));
            report.open();
            report.add(new Paragraph("Global Per USD Report Sample Text"));
            report.close();
            openReportPDF(filePath);
        } catch (Exception e){
            System.out.println("Failed to generate Global Per USD Report.");
            e.printStackTrace();
        }
    }

    public void openReportPDF(String filePath){
        //Checking to see if the user has a PDF Reader
        if (Desktop.isDesktopSupported()) {
            try{
                File report = new File(filePath);
                //Opening PDF file with PDF Reader
                Desktop.getDesktop().open(report);
                System.out.println("Report has been opened. \n File Location: " + filePath);
            } catch (IOException e){
                System.out.println("Failed to open report.");
                e.printStackTrace();
            }
        }
    }

//    public static void main(String[] args) {
//        try{
//            Document document = new Document();
//            PdfWriter.getInstance(document,new FileOutputStream("d:\\helloitext2.pdf"));
//            document.open();
//            document.add(new Paragraph("Oh my god Nick I did it it was so easy!!!"));
//            document.add(new Paragraph("DOWN WITH THE PATRIARCHY"));
//            document.close();
//        } catch (Exception e){
//            System.out.println("PDF was not created.");
//            e.printStackTrace();
//        }
//    }
}
