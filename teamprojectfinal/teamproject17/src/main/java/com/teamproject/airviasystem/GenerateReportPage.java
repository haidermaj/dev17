/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.teamproject.airviasystem;

/**
 *
 * @author Haider
 */
public class GenerateReportPage extends javax.swing.JFrame {
    Validator validate = new Validator();
    String[] inputFields = {"Lower Bound date", "Upper Bound date", "Report type"};
    int[] inputFieldsCharacterLimits = {10,10,255};
    /**
     * Creates new form GenerateReportPage
     */
    public GenerateReportPage() {
        initComponents();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        generateReportBGPanel = new javax.swing.JPanel();
        generateReportTitlePanel = new javax.swing.JPanel();
        generateReportTitle = new javax.swing.JLabel();
        generateReportDateLabel = new javax.swing.JLabel();
        generateReportDateLowerBoundField = new javax.swing.JFormattedTextField();
        generateReportSeparator = new javax.swing.JLabel();
        generateReportDateUpperBoundField = new javax.swing.JFormattedTextField();
        generateReportTypeBox = new javax.swing.JComboBox<>();
        generateReportChooseReportLabel = new javax.swing.JLabel();
        generateReportGenerateButton = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setResizable(false);

        generateReportBGPanel.setBackground(new java.awt.Color(52, 152, 219));

        generateReportTitlePanel.setBackground(new java.awt.Color(41, 128, 185));

        generateReportTitle.setFont(new java.awt.Font("Microsoft YaHei UI", 0, 48)); // NOI18N
        generateReportTitle.setForeground(new java.awt.Color(255, 255, 255));
        generateReportTitle.setText("Generate Report");

        javax.swing.GroupLayout generateReportTitlePanelLayout = new javax.swing.GroupLayout(generateReportTitlePanel);
        generateReportTitlePanel.setLayout(generateReportTitlePanelLayout);
        generateReportTitlePanelLayout.setHorizontalGroup(
            generateReportTitlePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(generateReportTitlePanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(generateReportTitle)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        generateReportTitlePanelLayout.setVerticalGroup(
            generateReportTitlePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(generateReportTitlePanelLayout.createSequentialGroup()
                .addGap(19, 19, 19)
                .addComponent(generateReportTitle)
                .addContainerGap(33, Short.MAX_VALUE))
        );

        generateReportDateLabel.setFont(new java.awt.Font("Microsoft YaHei UI Light", 0, 14)); // NOI18N
        generateReportDateLabel.setForeground(new java.awt.Color(255, 255, 255));
        generateReportDateLabel.setText("Date:");
        generateReportDateLabel.setToolTipText("");

        generateReportDateLowerBoundField.setBackground(new java.awt.Color(52, 152, 219));
        generateReportDateLowerBoundField.setBorder(javax.swing.BorderFactory.createMatteBorder(0, 0, 1, 0, new java.awt.Color(255, 255, 255)));
        generateReportDateLowerBoundField.setForeground(new java.awt.Color(255, 255, 255));
        generateReportDateLowerBoundField.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.DateFormatter(java.text.DateFormat.getDateInstance(java.text.DateFormat.SHORT))));
        generateReportDateLowerBoundField.setText("DD/MM/YYYY");
        generateReportDateLowerBoundField.setFont(new java.awt.Font("Microsoft YaHei UI Light", 0, 14)); // NOI18N
        generateReportDateLowerBoundField.setOpaque(false);

        generateReportSeparator.setFont(new java.awt.Font("Microsoft YaHei UI Light", 0, 14)); // NOI18N
        generateReportSeparator.setForeground(new java.awt.Color(255, 255, 255));
        generateReportSeparator.setText("-");
        generateReportSeparator.setToolTipText("");

        generateReportDateUpperBoundField.setBackground(new java.awt.Color(52, 152, 219));
        generateReportDateUpperBoundField.setBorder(javax.swing.BorderFactory.createMatteBorder(0, 0, 1, 0, new java.awt.Color(255, 255, 255)));
        generateReportDateUpperBoundField.setForeground(new java.awt.Color(255, 255, 255));
        generateReportDateUpperBoundField.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.DateFormatter(java.text.DateFormat.getDateInstance(java.text.DateFormat.SHORT))));
        generateReportDateUpperBoundField.setText("DD/MM/YYYY");
        generateReportDateUpperBoundField.setFont(new java.awt.Font("Microsoft YaHei UI Light", 0, 14)); // NOI18N
        generateReportDateUpperBoundField.setOpaque(false);

        generateReportTypeBox.setFont(new java.awt.Font("Microsoft YaHei UI Light", 0, 14)); // NOI18N
        generateReportTypeBox.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Report type...", "Individual - Interline", "Individual - Domestic", "Global - Interline Per USD", "Global - Interline Per Advisor", "Domestic", "Domestic - Per Advisor", "Stock Turnover" }));

        generateReportChooseReportLabel.setFont(new java.awt.Font("Microsoft YaHei UI Light", 0, 14)); // NOI18N
        generateReportChooseReportLabel.setForeground(new java.awt.Color(255, 255, 255));
        generateReportChooseReportLabel.setText("Choose Report:");
        generateReportChooseReportLabel.setToolTipText("");

        generateReportGenerateButton.setBackground(new java.awt.Color(52, 152, 219));
        generateReportGenerateButton.setFont(new java.awt.Font("Microsoft YaHei UI Light", 0, 14)); // NOI18N
        generateReportGenerateButton.setForeground(new java.awt.Color(255, 255, 255));
        generateReportGenerateButton.setText("Generate");
        generateReportGenerateButton.setBorder(null);
        generateReportGenerateButton.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        generateReportGenerateButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                generateReportGenerateButtonActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout generateReportBGPanelLayout = new javax.swing.GroupLayout(generateReportBGPanel);
        generateReportBGPanel.setLayout(generateReportBGPanelLayout);
        generateReportBGPanelLayout.setHorizontalGroup(
            generateReportBGPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(generateReportTitlePanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(generateReportBGPanelLayout.createSequentialGroup()
                .addGap(188, 188, 188)
                .addGroup(generateReportBGPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(generateReportGenerateButton)
                    .addGroup(generateReportBGPanelLayout.createSequentialGroup()
                        .addGroup(generateReportBGPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(generateReportDateLabel)
                            .addComponent(generateReportChooseReportLabel))
                        .addGap(80, 80, 80)
                        .addGroup(generateReportBGPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addGroup(generateReportBGPanelLayout.createSequentialGroup()
                                .addComponent(generateReportDateLowerBoundField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(generateReportSeparator)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(generateReportDateUpperBoundField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(generateReportTypeBox, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                .addContainerGap(225, Short.MAX_VALUE))
        );
        generateReportBGPanelLayout.setVerticalGroup(
            generateReportBGPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(generateReportBGPanelLayout.createSequentialGroup()
                .addGap(25, 25, 25)
                .addComponent(generateReportTitlePanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(166, 166, 166)
                .addGroup(generateReportBGPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(generateReportDateLabel)
                    .addComponent(generateReportDateLowerBoundField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(generateReportSeparator)
                    .addComponent(generateReportDateUpperBoundField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(generateReportBGPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(generateReportTypeBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(generateReportChooseReportLabel))
                .addGap(18, 18, 18)
                .addComponent(generateReportGenerateButton)
                .addContainerGap(193, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 800, Short.MAX_VALUE)
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addComponent(generateReportBGPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 600, Short.MAX_VALUE)
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addComponent(generateReportBGPanel, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void generateReportGenerateButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_generateReportGenerateButtonActionPerformed
        // TODO add your handling code here:
        String lowerBoundDate = generateReportDateLowerBoundField.getText(), upperBoundDate = generateReportDateUpperBoundField.getText(),
                reportType = generateReportTypeBox.getSelectedItem().toString();
        if(validate.isInputLengthValid(inputFieldsCharacterLimits, inputFields, lowerBoundDate, upperBoundDate,reportType)){
            if(!(validate.isDateValid(lowerBoundDate))){
                System.out.println("Lower bound date invalid");
            } else if(!(validate.isDateValid(upperBoundDate))){
                System.out.println("Upper bound date invalid");
            } else if(!(validate.isComboBoxSelected("Report type", reportType,"Report type..."))) {
                System.out.println("Report type invalid");
            } else {
                System.out.println("Do something");
            }
        }
    }//GEN-LAST:event_generateReportGenerateButtonActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(GenerateReportPage.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(GenerateReportPage.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(GenerateReportPage.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(GenerateReportPage.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new GenerateReportPage().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel generateReportBGPanel;
    private javax.swing.JLabel generateReportChooseReportLabel;
    private javax.swing.JLabel generateReportDateLabel;
    private javax.swing.JFormattedTextField generateReportDateLowerBoundField;
    private javax.swing.JFormattedTextField generateReportDateUpperBoundField;
    private javax.swing.JButton generateReportGenerateButton;
    private javax.swing.JLabel generateReportSeparator;
    private javax.swing.JLabel generateReportTitle;
    private javax.swing.JPanel generateReportTitlePanel;
    private javax.swing.JComboBox<String> generateReportTypeBox;
    // End of variables declaration//GEN-END:variables
}
